﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCStateManagement.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Test(int? id, int? comm)
        {
            ViewBag.id = id;
            ViewBag.comm = comm;

            HttpCookie cookie = new HttpCookie("test");

            if (comm.HasValue)
            {
                cookie.Values["comm"] = comm.ToString();
                cookie.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(cookie);
            }

            cookie = Request.Cookies["test"];
            string fromCookie = cookie?.Values["comm"] ?? "puudub";
            ViewBag.fromCookie = fromCookie;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            ViewData["Nimi"] = "Henn on tore poiss";
            ViewBag.Number = 7;

            dynamic TempBag = new UniversalBag(TempData);

            TempBag.Nimi = "Henn";
            TempBag.Vanus = 62;

            ViewBag.Temp = TempBag;
           

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}