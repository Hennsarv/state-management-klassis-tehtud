﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCStateManagement.Models
{
    public class Inimene
    {

        static Inimene()
        {
            TeeMõnedInimesed();
        }

        public static void TeeMõnedInimesed()
        {
            new Inimene { Nimi = "Henn", Vanus = 62 };
            new Inimene { Nimi = "Ants", Vanus = 40 };
            new Inimene { Nimi = "Peeter", Vanus = 28 };

        }

        public static Dictionary<int, Inimene> Inimesed = new Dictionary<int, Inimene>();
        static int loendur = 0;
        public int Id;
        public string Nimi;
        public int Vanus;

        public Inimene()
        {
            Id = ++loendur;
            Inimesed.Add(Id, this);
        }

    }
}