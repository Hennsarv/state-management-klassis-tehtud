﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;


public class UniversalBag : DynamicObject
{
    IDictionary<string, object> prop;

    public UniversalBag(IDictionary<string, object> outerBag)
    {
        this.prop = outerBag;
    }

    public override bool TrySetMember(SetMemberBinder binder, object value)
    {
        prop[binder.Name] = value;
        return true;
    }

    public override bool TryGetMember(GetMemberBinder binder, out object result)
    {
        result = prop[binder.Name];
        return true;
    }
}


