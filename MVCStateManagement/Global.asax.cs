﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MVCStateManagement
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Application["Started"] = DateTime.Now.ToLongTimeString();
            Application["Nimi"] = "Henn ise tegi";
            Application["Number"] = 7;

        }

        protected void Session_Start()
        {
            Session["Started"] = DateTime.Now.ToLongTimeString();
            Session["Nimi"] = "üks session";
            Session["Number"] = 0;
        }

    }
}
